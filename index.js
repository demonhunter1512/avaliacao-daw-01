const express = require('express')
const app = express();
var fs = require('fs')

app.use(express.static(__dirname + '/public'));
app.use(express.urlencoded({ extended: true }))

app.use(express.static('public'));

app.get('/', (req, res) => {
    res.sendFile(__dirname + "/public/html/index.html")

})

app.get('/eu', (req, res) => {
    res.sendFile(__dirname + "/public/html/eu.html")
})

app.get('/duvida', (req, res) => {
    res.sendFile(__dirname + "/public/html/duvida.html")
})


app.get('/confirmacao', function(req, res) {
    var em = req.query.emailForm
    var des = req.query.descricaoForm
    res.send("A dúvida "+des+" foi enviada com sucesso e será respondida no e-mail: "+em);
  });

app.get('/aluno', (req, res) => {
    res.sendFile(__dirname + "/public/html/aluno.html")
})


/*const fs = require('fs')

let alunoCru = fs.readFileSync(__dirname + '/alunos.json')
let alunos = JSON.parse(alunoCru)

app.get('/teste',(req,res) =>{
    res.send(alunos)
})*/



app.listen(3000);